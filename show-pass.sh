#!/bin/bash

# Encrypt string
# ansible-vault encrypt_string 'secret-decoder-ring' --name 'the_secret'

ansible all -m debug -a "var='the_secret'" --ask-vault-pass
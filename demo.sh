#!/bin/bash

# Sample Ad-Hoc Commands
# ansible all -m ping
# ansible all -a uptime # default module 'command'

# Playbook, Short Form:
# ansible-playbook -l web -k -K sample-deploy.yml

ansible-playbook          \
    --limit=web           \
    --ask-pass            \
    --ask-become-pass     \
    sample-deploy.yml

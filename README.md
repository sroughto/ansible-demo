# README #

Supplemental files for Intro to Ansible, CU OIT Tech Talks, April 18, 2018.

## Summary ##

* This repository contains demonstration ansible scripts to accompany the presentation
 "Intro to Ansible," CU OIT Tech Talks, April 18, 2018.

## Prerequisites ##

To deploy the sample site, you'll need to be on the CU OIT VPN with access to CU's Artifactory.

## Setup ##

### How to Setup sample Vagrant Boxes ###

* Download and install VirtualBox and Vagrant.
* Create a directory to hold Vagrantfiles, etc.
* Install box images.

```bash
vagrant box add --name ubuntu ubuntu/trusty64
vagrant box add --name centos \
    https://github.com/2creatives/vagrant-centos/releases/download/v6.4.2/centos64-x86_64-20140116.box
```

* For each box, create a subdirectory and configure the box.

```bash
cd <vagrantdirectory>
mkdir ubuntu
cd ubuntu
vagrant init ubuntu/trusty64
```

* Add specific port mappings to each Vagrantfile.

```text
config.vm.network :forwarded_port, guest: 22, host: 2203, id: 'ssh'
config.vm.network "forwarded_port", guest: 80, host: 8083, host_ip: "127.0.0.1"
```

* Startup the box.

```bash
* vagrant up
```

* Repeat for other boxes.

## Demonstration Points ##

_Some things to try with this demo._

### Inventory ###

* [ ] Walk through the inventory file.
* [ ] Try commands like ```ansible web --list-hosts``` to view hosts in various groups.
* [ ] Try ansible-console (uptime, reboot, w).
* [ ] Reboot will fail: add command-line args ```-k -K --become```

### Playbook ###

* [ ] Walkthrough ```sample-deploy.yml```.
* [ ] Note variable definitions and scope.
* [ ] Note nginx role and tags.
* [ ] Note block and tags.
* [ ] Walkthrough block tasks.
* [ ] Describe delegate_to, run_once.
* [ ] Run ```ansible-lint``` against the playbook.
* [ ] Run the playbook ```ansible-playbook -k -K sample-deploy.yml```.
* [ ] Run it again to see nothing change.

### Inventory Directory ###

* [ ] Create directory inventory.d.
* [ ] Move inventory, host_vars, group_vars into inventory.d.
* [ ] Extract web group into a separate file.
* [ ] Point ansible.cfg at inventory.d.
* [ ] Run ```ansible all -m ping```, etc.
* [ ] Note that this directory can contain scripts.

### Add webserver ###

* [ ] Add centos2 to web group.
* [ ] Run playbook (```./demo.sh```).
* [ ] Now centos2 matches the other web hosts.

### Extra Credit ###

* [ ] Uncomment final tasks in the playbook.
* [ ] Note the ```never``` tag (introduced in Ansible 2.5).
* [ ] Run playbook with ```--tags ps```
* [ ] Run playbook with ```--tags nginx``` to update index.html.
* [ ] Try the display-fact.yml playbook.
* [ ] Try ```show-pass.sh``` to check out *Vault*.
* [ ] Note the inventory 'all' group list object ```package```, and per-group values
  for the list item package.emacs.
* [ ] Install emacs: ```ansible all -m package -a 'name={{ package.emacs }} state=present' -k -K -b```

## Additional Resources ##

* Lynda Tutorials
  * <https://www.lynda.com/Ansible-tutorials/>

* _What is Ansible, how it works and how it works for me_
  * by Ivica Kolenkaš
  * <https://www.linkedin.com/pulse/what-ansible-how-works-me-ivica-kolenka%C5%A1>

* _Mastering Ansible_
  * by Jesse Keating
  * Available on Amazon

## Contact ##

* Steve Roughton steve.roughton@colorado.edu
* Development and Architecture, CU OIT